#include "Pins.h"

#include "adc.h"
#include "bluetooth.h"
#include "debug.h"
#include "DHT11.h"
#include "ledHandler.h"
#include "rtc.h"
#include "sensors.h"
#include "boolean.h"
#include "ringbuffer.h"

#include "globalDefines.h"

// 'Private' declarations

#define DEBUG_FLAG			0x01
#define BLUETOOTH_FLAG		0x02
#define RTC_FLAG			0x04

RingBuffer _main_debugMessageBuffer = { { 0 }, 0, 0 };
RingBuffer _main_bluetoothMessageBuffer = { { 0 }, 0, 0 };
int8_t interruptDrivenFlags = 0;	// 0x01 = debug message received, 0x02 = bluetooth message received, 0x04 = power off (see defines above)

void _main_initializeClock(void);
void _main_powerOff(void);
void _main_powerOn(void);

void _main_addCharacterToBluetoothMessageBuffer(char character);
void _main_addCharacterToDebugMessageBuffer(char character);
void _main_getMeasuredValuesAndSendOverBluetooth(void);
void _main_handleMessage(RingBuffer *ring);
Bool _main_stringCompare(const char* stringToCompare, const char* stringToCompareAgainst);	// This helper function returns more intuitive values than strcmp (0 = False = strings are not identical, 1 = True = strings are identical)
void _main_waitForOscillatorReady(void);

int main(void){
	PORTE.DIRSET = POWER_EN;	// Set direction of Board power enable pin
	_main_powerOn();
	PORTC.DIRSET = PIN3_bm;		// Set direction of Bluetooth pin
	PORTB.DIRSET = HY_EN;		// Set direction of Heater power enable pin

	initializeLedHandler();
	turnLedOnWithColor(Red);	// Red = initializing

	powerOnHeater();			// Heater should be always-on in new design...
	_main_initializeClock();
	initializeADC();	
	initializeRTC();
	initializeDebug();
	powerOnBT();
	initializeBluetooth();
	
	initializeSensors();
	
	PORTC.DIRCLR = USB_STAT;
	
	#ifndef SIMULATION
	stdout = &mystdout;	// Use custom 'stdout' (USART)
	#endif

	//sei();				// Enable interrupts
	
	PMIC.CTRL |= (PMIC_HILVLEN_bm | PMIC_LOLVLEN_bm);		// High en Low level interrupts
	
	sei();

	// BT config
	PORTC.DIRCLR = BT_STAT;
	
	PORTC.DIRSET = BT_KEY;
	PORTC.OUTCLR = BT_KEY;
	
	PORTC.DIRSET = BT_EN;
	PORTC.OUTSET = BT_EN;
	
	PORTB.DIRSET = HY_EN;
	
	// These calls make the simulator hang
	#ifndef SIMULATION
	bluetoothRename("SnuffelneusRefactoredEdition");
	
	_delay_ms(2000);
	#endif

	turnLedOnWithColor(Blue);
	printf("Initialization Complete. Entering main loop...\r\n\r\n");

	while(1){
		if(interruptDrivenFlags & DEBUG_FLAG){
			interruptDrivenFlags ^= DEBUG_FLAG;	// Reset debug received flag

			_main_handleMessage(&_main_debugMessageBuffer);
			clearBuffer(&_main_debugMessageBuffer);
		}

		if(interruptDrivenFlags & BLUETOOTH_FLAG){
			interruptDrivenFlags ^= BLUETOOTH_FLAG;	// Reset bluetooth received flag

			_main_handleMessage(&_main_bluetoothMessageBuffer);
			clearBuffer(&_main_bluetoothMessageBuffer);
		}

		if(interruptDrivenFlags & RTC_FLAG){
			interruptDrivenFlags ^= RTC_FLAG;	// Reset RTC compare flag

			_main_powerOff();
		}


		// TEST - Enabling this code will send measurement data over USART every 3 seconds.
		/*turnLedOnWithColor(Yellow);
		performMeasurement();
		resetMillisecondCounter();
		printf("ADC Sensor Value: %.4f\n", getSensorValue());
		turnLedOnWithColor(Blue);
		_delay_ms(3000);*/
		// TEST END
	}
}

void _main_powerOn(void){
	PORTE.OUTSET = POWER_EN;
}

void _main_initializeClock(void) {
	CCP = CCP_IOREG_gc;              // Disable register security for oscillator update
	OSC.CTRL = OSC_RC32MEN_bm;       // Enable 32MHz oscillator

	_main_waitForOscillatorReady();

	CCP = CCP_IOREG_gc;              // Disable register security for clock update
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // Switch to 32MHz clock
}

void _main_waitForOscillatorReady(void){
	while(!(OSC.STATUS & OSC_RC32MRDY_bm));
}

void _main_handleMessage(RingBuffer* messageBuffer) {
	char* currentToken = strtok(messageBuffer->buffer, "-");

	for(; currentToken; currentToken = strtok(NULL, " ")){
		if(_main_stringCompare(currentToken, "wakeup")){		// TODO: Change to new command: 'measure'
			printf("Command \'measure\' received and executing\r\n");

			turnLedOnWithColor(Yellow);
			_main_getMeasuredValuesAndSendOverBluetooth();
			turnLedOnWithColor(Blue);
		}
		else if(_main_stringCompare(currentToken, "shutdown")){
			printf("Command \'shutdown\' received and executing\r\n");

			_main_powerOff();
		}
		else if(_main_stringCompare(currentToken, "setname")){
			printf("Command \'setname\' received and executing\r\n");

			char* newName = strtok(NULL, " ");

			char bluetoothMessage[80];
			sprintf(bluetoothMessage, "AT+NAME%s", newName);

			bluetoothSendString(bluetoothMessage);
		}
	}
}

Bool _main_stringCompare(const char* stringToCompare, const char* stringToCompareAgainst){
	return strcmp(stringToCompare, stringToCompareAgainst) ? False : True;
}

void _main_getMeasuredValuesAndSendOverBluetooth(void){
	printf("Measuring and sending results over Bluetooth...\r\n");

	performMeasurement();
	resetMillisecondCounter();
	
	int measuredTemperature = getTemperature();
	int measuredHumidity = getHumidity();
	double measuredSensorValue = getSensorValue();
	double measuredInternalBatteryVoltage = getInternalBatteryVoltage();
	double measuredExternalBatteryVoltage = getExternalBatteryVoltage();

	//Format : sensorValue--temperature--humidity--battery1--battery2-EOM (end of message)
	char bluetoothMessage[80];
	sprintf(bluetoothMessage, "Result--%.4f--%d--%d--%f--%f--EOM", measuredSensorValue, measuredTemperature, measuredHumidity, measuredInternalBatteryVoltage, measuredExternalBatteryVoltage);

	bluetoothSendString(bluetoothMessage);

	printf("Message sent: %s\r\n", bluetoothMessage);
	printf("ADC Sensor Value: %.4f\n", getSensorValue());
}

void _main_powerOff(void){
	PORTE.OUTCLR = POWER_EN;
}

// RTC compare interrupt (should be triggered every millisecond, is currently triggered every 10ms)
ISR(RTC_COMP_vect) {
	incrementMillisecondCounter();

	if(getMillisecondCounter() >= INACTIV_TIMEOUT) {
		interruptDrivenFlags |= RTC_FLAG;	// Set RTC compare flag
	}
}

// Interrupt routine usartC1 RX (Debug - character received)
ISR(USARTC1_RXC_vect) {
	char c = USARTC1.DATA;	// Get received character
	
	_main_addCharacterToDebugMessageBuffer(c);

	if(c == 0x0D){
		interruptDrivenFlags |= DEBUG_FLAG;	// Set debug received flag
	}
}

void _main_addCharacterToDebugMessageBuffer(char character){
	if(character == 0x0D) {		//Return. works with Putty, not with Hyperterminal
		character = '\0';
	}

	addCharacterToBuffer(character, &_main_debugMessageBuffer);
}

// Interrupt routine usartE0 RX (Bluetooth - character received)
ISR(USARTE0_RXC_vect) {
	char c = USARTE0.DATA;	// Get received character
	
	_main_addCharacterToBluetoothMessageBuffer(c);

	if(c == '\n'){
		interruptDrivenFlags |= BLUETOOTH_FLAG;	// Set bluetooth message received flag
	}
}

void _main_addCharacterToBluetoothMessageBuffer(char character){
	if(character == '\n') {
		character = '\0';
	}

	addCharacterToBuffer(character, &_main_bluetoothMessageBuffer);
}