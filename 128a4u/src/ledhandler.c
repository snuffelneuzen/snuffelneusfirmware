#include "ledHandler.h"

void initializeLedHandler(void){
	PORTD.DIRSET = LED;
	PORTD.DIRSET = RED;
	PORTD.DIRSET = GREEN;
	PORTD.DIRSET = BLUE;
}

void turnLedOff(void){
	PORTD.OUTSET = RED | GREEN | BLUE;
}

void turnLedOnWithColor(LedColor color){
	turnLedOff();
	
	switch (color)	{
		case Red:
			PORTD.OUTCLR = RED;
			break;
		case Green:
			PORTD.OUTCLR = GREEN;
			break;
		case Blue:
			PORTD.OUTCLR = BLUE;
			break;
		case Yellow:
			PORTD.OUTCLR = RED | GREEN;
			break;
		case Pink:
			PORTD.OUTCLR = RED | BLUE;
			break;
		case Cyan:
			PORTD.OUTCLR = GREEN | BLUE;
			break;
		case White:
			PORTD.OUTCLR = RED | GREEN | BLUE;
			break;
	}
}