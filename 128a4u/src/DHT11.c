#include "DHT11.h"

// 'Private' declarations

#define DHT_TIMEOUT 1000

void initializeDHT11(void){
	PORTB.DIRSET = SEN1_EN;
	PORTB.OUTSET = SEN1_EN;
}

Bool readDHT11(int* temperatureOutParam, int* humidityOutParam){
	uint8_t i,j = 0;

	//reset port
	DHT_DATA_PORT.DIRSET = DHT_DATA_PIN;
	DHT_DATA_PORT.OUTSET = DHT_DATA_PIN;
	
	_delay_ms(200);

	//send request
	DHT_DATA_PORT.DIRSET = DHT_DATA_PIN;
	DHT_DATA_PORT.OUTCLR = DHT_DATA_PIN;

	_delay_ms(18);

	DHT_DATA_PORT.OUTSET = DHT_DATA_PIN;

	_delay_us(40);

	DHT_DATA_PORT.DIRCLR = DHT_DATA_PIN;

	// ACKNOWLEDGE or TIMEOUT
	unsigned int loopCount = 10000;
	while(!(DHT_DATA_PORT.IN & DHT_DATA_PIN)){
		if (loopCount-- == 0){
			return False;
		}
	}

	loopCount = 10000;
	while((DHT_DATA_PORT.IN & DHT_DATA_PIN)){
		if (loopCount-- == 0){
			return False;
		}
	}

	uint8_t bits[5];
	memset(bits, 0, sizeof(bits));

	// Read the data
	uint16_t timeoutcounter = 0;
	for (j = 0; j < 5; j++) {		// Read 5 byte
		uint8_t result = 0;

		for(i = 0; i < 8; i++) {	// Read all bits
			timeoutcounter = 0;

			while(!(DHT_DATA_PORT.IN & DHT_DATA_PIN)) {		// Wait for a high input (non blocking)
				timeoutcounter++;

				if(timeoutcounter > DHT_TIMEOUT) {
					return False;		// Timeout
				}
			}
			
			_delay_us(30);

			if(DHT_DATA_PORT.IN & DHT_DATA_PIN){		// If input is high after 30 us, get result
				result |= (1<<(7-i));
			}

			timeoutcounter = 0;

			while(DHT_DATA_PORT.IN & DHT_DATA_PIN) {	// Wait until input gets low (non blocking)
				timeoutcounter++;

				if(timeoutcounter > DHT_TIMEOUT) {
					return False;						// Timeout
				}
			}
		}
		
		bits[j] = result;
	}
	
	uint8_t firstThreeResultBits = (uint8_t)(bits[0] + bits[1] + bits[2] + bits[3]);

	if(firstThreeResultBits == bits[4]) {		// Checksum
		*temperatureOutParam = bits[2];
		*humidityOutParam = bits[0];
		
		return True;
	}
	
	return False;
}