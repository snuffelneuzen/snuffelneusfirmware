#ifndef GLOBALDEFINES_H_
#define GLOBALDEFINES_H_

#include "Pins.h"

//#define SIMULATION

#define USART_BAUDRATE 9600

#ifdef F_CPU
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1) //Macro om de baud te prescalen met de clock (F_CPU) rate (?)
#endif

#endif