#ifndef RTC_H_
#define RTC_H_

#include "pins.h"

// Please note: RTC is a COUNTER, not a CLOCK!
// It is currently set to trigger an interrupt every 10ms

void initializeRTC(void);

unsigned long getMillisecondCounter(void);
void incrementMillisecondCounter(void);
void resetMillisecondCounter(void);

#endif