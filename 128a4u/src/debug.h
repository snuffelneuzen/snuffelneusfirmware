#ifndef DEBUG_H_
#define DEBUG_H_

#include "pins.h"

void initializeDebug(void);

// Print character (@see printf)
void debugPutChar(char c);

#endif