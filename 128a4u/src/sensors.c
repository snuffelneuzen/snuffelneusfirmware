#include "sensors.h"

// 'Private' declarations

double _sensors_externalBatteryValue;
int _sensors_humidity;
double _sensors_internalBatteryValue;
Bool _sensors_isHeaterOn;
double _sensors_sensorValue;
int _sensors_temperature;

void _sensors_initializeSensor2(void);

void initializeSensors(void){
	initializeDHT11();
	_sensors_initializeSensor2();
}

void _sensors_initializeSensor2(void){
	PORTB.DIRSET = SEN2_EN;
	PORTB.OUTSET = SEN2_EN;
}

void powerOffHeater(void){
	PORTB.OUTCLR = HY_EN;
}

void powerOnHeater(void){
	PORTB.OUTSET = HY_EN;
}

void performMeasurement(void){
	int dht11Temperature;
	int dht11Humidity;

	Bool dht11ReturnValue = readDHT11(&dht11Temperature, &dht11Humidity);
	
	if(dht11ReturnValue == False) {
		printf("DHT11 measurement unsuccessful. No new values were stored.\r\n");
		
		// TODO: Handle faulty measurement?
	}
	else{
		_sensors_temperature = dht11Temperature;
		_sensors_humidity = dht11Humidity;
	}
	
	_sensors_internalBatteryValue = readInternalBattery();
	_sensors_externalBatteryValue = readExternalBattery();

	double sensorValue = 0;
	int i;

	for(i = 0; i < 15; i++) {
		sensorValue += readSensor();
	}
	
	sensorValue /= i;

	_sensors_sensorValue = sensorValue;
	
	printf("ADC's ruw: 1 (%.0f), 2 (%.0f), 3 (%.0f), 4 (%.0f)\r\n", adc_value, adc2, adc3, adc4);
	printf("ADC's : 1 (%f), 2 (%f), 3 (%f), 4 (%f)\r\n", ((adc_value * VREFB_VALUE) / 4096), ((adc2 * VREFB_VALUE) / 4096), ((adc3 * VREFB_VALUE) / 4096), ((adc4 * VREFB_VALUE) / 4096));
	printf("VSEN : %f, VRES : %f\r\n", vsen, vres);	
}

double getExternalBatteryVoltage(void){
	return _sensors_externalBatteryValue;
}

int getHumidity(void){
	return _sensors_humidity;
}

double getInternalBatteryVoltage(void){
	return _sensors_internalBatteryValue;
}

Bool getIsHeaterOn(void){
	return _sensors_isHeaterOn;
}

double getSensorValue(void){
	return _sensors_sensorValue;
}

int getTemperature(void){
	return _sensors_temperature;
}