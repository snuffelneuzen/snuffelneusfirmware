#ifndef SENSORS_H_
#define SENSORS_H_

#include "adc.h"
#include "boolean.h"
#include "DHT11.h"

void initializeSensors(void);
void powerOffHeater(void);
void powerOnHeater(void);

void performMeasurement(void);

double getExternalBatteryVoltage(void);
int getHumidity(void);
double getInternalBatteryVoltage(void);
Bool getIsHeaterOn(void);
double getSensorValue(void);
int getTemperature(void);

#endif