#include "adc.h"
int8_t offset;

void initializeADC(void) {
	/* Get ADCACAL0 from production signature . */
	ADCA.CALL = ReadCalibrationByte( PROD_SIGNATURES_START + ADCACAL0_offset );
	ADCA.CALH = ReadCalibrationByte( PROD_SIGNATURES_START + ADCACAL1_offset );

	/* Set up ADC B to have signed conversion mode and 12 bit resolution. */
	ADCA.CTRLB = ADC_RESOLUTION_12BIT_gc;					// 12 bit right adjusted results
	ADCA.CTRLB |= (0x01<<4);								// Signed conversionmode

	// The ADC has different voltage reference options, controlled by the REFSEL bits in the
	// REFCTRL register. Here the internal reference is selected
	ADCA.REFCTRL = ADC_REFSEL_INTVCC_gc; //Internal VCC / 1.6 reference voltage -> This gives a reference of about 2V, which happened to be the maximum value during unittesting...

	// The clock into the ADC decide the maximum sample rate and the conversion time, and
	// this is controlled by the PRESCALER bits in the PRESCALER register. Here, the
	// Peripheral Clock is divided by 8 ( gives 250 KSPS with 2Mhz clock )
	ADCA.PRESCALER = ADC_PRESCALER_DIV8_gc;			// Divide peripheral clock by 8

	ADCA.CH0.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;	// Set channel 0 single ended
	ADCA.CH1.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;	// Set channel 1 single ended
	ADCA.CH2.CTRL = ADC_CH_INPUTMODE_DIFF_gc;			// Set channel 0 differential
	
	// Setting up the which pins to convert.
	// Note that the negative pin is internally connected to ground
	ADCA.CH0.MUXCTRL = ADC_CH_MUXNEG_PIN0_gc;
	ADCA.CH0.MUXCTRL |= ADC_CH_MUXPOS_PIN1_gc;
	
	ADCA.CH1.MUXCTRL = ADC_CH_MUXNEG_PIN0_gc;
	ADCA.CH1.MUXCTRL |= ADC_CH_MUXPOS_PIN2_gc;

	// Before the ADC can be used it must be enabled
	ADCA.CTRLA |= 0x1;																	/* enable ADC */	
												
	/* Store old prescaler value. */
	uint8_t prescaler_val = ADCA.PRESCALER;

	/* Set prescaler value to minimum value. */
	ADCA.PRESCALER = ADC_PRESCALER_DIV4_gc;

	/* Wait 4*COMMON_MODE_CYCLES for common mode to settle. */
	_delay_us(4*COMMON_MODE_CYCLES);

	/* Set prescaler to old value*/
	ADCA.PRESCALER = prescaler_val;

	/*Calculate ground offset*/
	ADCA.CH0.CTRL |= ADC_CH_START_bm;
	offset = ADCA.CH0.CTRL;
	
	do{
	} while (!(ADCA.CH0.INTFLAGS & ADC_CH_CHIF_bm) != 0x00);
	
	uint16_t answer;

	/* Clear interrupt flag.*/
	ADCA.CH0.INTFLAGS = ADC_CH_CHIF_bm;

	/* Return result register contents*/
	answer = ADCA.CH0.RES;
	offset = answer;
	
	_delay_us(2);																		/* Laten stabiliseren */
}

double readInternalBattery(void) {
	/* Read small battery*/
	double v_vbatt, vbatt;
	
	ADCA.CH0.CTRL |= ADC_CH_START_bm;	// Start sampling
	
	while(!ADCA.CH0.INTFLAGS);															/* wait for conversion complete flag */
	
	ADCA.CH0.INTFLAGS =  ADC_CH_CHIF_bm;
	
	v_vbatt = (double) ADCA.CH0RES;
	
	vbatt = CALCULATE_VBATT(v_vbatt); //VBATT
	printf("Accu : %.0f, %f\r\n", v_vbatt, vbatt);
	return vbatt;
}

double readExternalBattery(void) {
	/* Read big battery */
	double v_vbatth, vbatth;
	
	ADCA.CH1.CTRL |= ADC_CH_START_bm;	// Start sampling
	
	while(!ADCA.CH1.INTFLAGS);															/* wait for conversion complete flag */
	ADCA.CH1.INTFLAGS = ADC_CH_CHIF_bm;
	v_vbatth = (double) ADCA.CH1RES - gADC_CH0_ZeroOffset;
	
	vbatth = CALCULATE_VBATT(v_vbatth); //VBATTH
	printf("12V Accu : %.0f, %f, %f\r\n", v_vbatth, vbatth, (double) ADCA.CH1RES);
	return vbatth;
	
}

double readSensor(void) {
	ADCA.CH2.MUXCTRL = ADC_CH_MUXPOS_PIN4_gc;  // set positive pin
	ADCA.CH2.MUXCTRL |= ADC_CH_MUXNEG_PIN1_gc; // set negative pin
	
	ADCA.CH2.CTRL |= ADC_CH_START_bm; // start reading
	
	while(!ADCA.CH2.INTFLAGS);														/* wait for conversion complete flag */

	ADCA.CH2.INTFLAGS = ADC_CH_CHIF_bm;

	unsigned short testAdcVal = (unsigned short)ADCA.CH2RES;
	signed short testAdcValSigned = 0;

	// If the result is negative, apply two's complement to make the value positive (for this application what matters is the difference, not whether is positive of negative)
	if((testAdcVal & 0b0000100000000000) != 0){
		testAdcValSigned = (~testAdcVal) + 1;
	}
	else{
		testAdcValSigned = (testAdcVal & 0b0000011111111111);		// Only use the 11 bits with the result if the result is positive. The first 5 bits indiciate the sign (positive/negative)
	}

	printf("ADC Signed Test: %d\n", testAdcValSigned);
	printf("adc: %f - %f\r\n", adc_value, CALCULATE_ADC(adc_value));

	return testAdcValSigned;
}

/*! \brief Function for GCC to read out calibration byte.
 *
 *  \note For IAR support, include the adc_driver_asm.S90 file in your project.
 *
 *  \param index The index to the calibration byte.
 *
 *  \return Calibration byte.
 *  \author Atmel Corporation: http://www.atmel.com
 */
uint8_t ReadCalibrationByte(uint8_t index)
{
	uint8_t result;

	/* Load the NVM Command register to read the calibration row. */
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	result = pgm_read_byte(index);

	/* Clean up NVM Command register. */
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;

	return result;
}