#ifndef DHT11_H_
#define DHT11_H_

#include "Pins.h"

#include "boolean.h"

void initializeDHT11(void);
Bool readDHT11(int* temperatureOutParam, int* humidityOutParam);

#endif