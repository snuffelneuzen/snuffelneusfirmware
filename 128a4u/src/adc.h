#ifndef ADC_H_
#define ADC_H_

#include "pins.h"

#define TOP 2047
#define CALCULATE_ADC(I) (double) (((I) * VREFB_VALUE) / (TOP+1))
#define CALCULATE_VBATT(I) (double) (((I) * VREFB_VALUE) / (TOP+1))

#define CALCULATE_VBATTH(I) (double) ((I * VREFBATH_VALUE) / TOP)
#define CALCULATE_ADC_DIFF(I, J) (double) (((I - J) * VREFB_VALUE) / (TOP+1))
#define CALCULATE_RSEN(VRES, VSEN) (double) (VSEN / (VRES / 5360))

#define COMMON_MODE_CYCLES 16

double adc_value, adc2, adc3, adc4, vres, vsen;

int gADC_CH0_ZeroOffset;

void initializeADC(void);

void calculateGroundOffset(void);

double readInternalBattery(void);

double readExternalBattery(void);

double readSensor(void);

uint8_t ReadCalibrationByte(uint8_t index);

/* Offset addresses for production signature row on GCC */
#ifndef ADCACAL0_offset

#define ADCACAL0_offset 0x20
#define ADCACAL1_offset 0x21
#define ADCBCAL0_offset 0x24
#define ADCBCAL1_offset 0x25

#endif

#endif /* ADC_H_ */