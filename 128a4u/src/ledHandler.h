#ifndef LEDHANDLER_H_
#define LEDHANDLER_H_

#include "pins.h"

typedef enum{ Red, Green, Blue, Yellow, Pink, Cyan, White } LedColor;

void initializeLedHandler(void);

void turnLedOff(void);
void turnLedOnWithColor(LedColor color);	// Can be used consecutively without calling turnLedOff().

#endif