#include "bluetooth.h"

#include "globalDefines.h"

// 'Private' declarations

void _bt_bluetoothPutChar(char c);		// Print character (@see printf)
void _bt_waitForEmptyTransmitBuffer(void);

void initializeBluetooth(void) {
	// Set Tx pin to an output
	PORTE.DIRSET = PIN3_bm;

	USARTE0.BAUDCTRLB = 0;
	USARTE0.BAUDCTRLA = BAUD_PRESCALE;
	
	// Set Control Reg C
	// CMODE = 00 (Async USART)
	// PMODE = 00 (No Parity) (10 = Even, 11 = Odd)
	// SBMODE = 0 (1-Stop) (1 = 2-Stop Bits)
	// CHSIZE = 011 (8bit)
	USARTE0.CTRLC = (USARTE0.CTRLC & ~USART_CHSIZE_gm) | USART_CHSIZE_8BIT_gc;

	// Set Control Reg B
	// Enable Rx and Tx
	USARTE0.CTRLB = (USART_RXEN_bm | USART_TXEN_bm);
	
	// Setup interrupt?
	USARTE0.CTRLA = USART_RXCINTLVL_HI_gc;
}

void powerOffBT(void) {
	PORTC.OUTCLR = PIN3_bm;
}

void powerOnBT(void) {
	PORTC.OUTSET = PIN3_bm;
}

void bluetoothRename(char* name) {
	bluetoothSendString("AT+NAME");
	bluetoothSendString(name);
}

void bluetoothSendString(char* string) {
	while (*string) {
		_bt_bluetoothPutChar(*string++);
	}
}

// Write character over Bluetooth
void _bt_bluetoothPutChar(char c) {
	_bt_waitForEmptyTransmitBuffer();

	USARTE0.DATA = c;
}

void _bt_waitForEmptyTransmitBuffer(void){
	while (!(USARTE0.STATUS & USART_DREIF_bm));
}