#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#include "pins.h"

void initializeBluetooth(void);
void powerOffBT(void);
void powerOnBT(void);

void bluetoothRename(char* name);
void bluetoothSendString(char* string);

#endif