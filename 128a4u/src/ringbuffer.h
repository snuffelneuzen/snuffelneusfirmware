#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#define RX_BUFFER_SIZE 128

struct RingBuffer {
	char buffer[RX_BUFFER_SIZE];
	int head;
	int tail;
};

typedef struct RingBuffer RingBuffer;

void addCharacterToBuffer(char characterToAdd, RingBuffer* bufferToAddTo);
void clearBuffer(RingBuffer* bufferToClear);

#endif