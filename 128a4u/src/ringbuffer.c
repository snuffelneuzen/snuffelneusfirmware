#include "ringbuffer.h"

#include <string.h>

void addCharacterToBuffer(char characterToAdd, RingBuffer* bufferToAddTo)
{
	int newHead = (bufferToAddTo->head + 1) % RX_BUFFER_SIZE;

	// Only add character if it does not cause a buffer overflow
	if (newHead != bufferToAddTo->tail) {
		bufferToAddTo->buffer[bufferToAddTo->head] = characterToAdd;
		bufferToAddTo->head = newHead;
	}
}

void clearBuffer(RingBuffer* bufferToClear){
		memset(bufferToClear->buffer, 0, sizeof(bufferToClear->buffer));
		bufferToClear->head = 0;
		bufferToClear->tail = 0;
}