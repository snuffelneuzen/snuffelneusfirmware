#include "rtc.h"

#include "globalDefines.h"

// 'Private' declarations

volatile unsigned long _rtc_millisecondCounter = 0;

void _rtc_config32KHzRTC(void);
void _rtc_waitUntilSynchronizationCompleted(void);

void initializeRTC(void) {
	_rtc_config32KHzRTC();

	RTC.PER = RTC_MILLISECONDS - 1;		// Overflow period
	RTC.CNT = 0;
	RTC.COMP = RTC_MILLISECONDS - 1;
	RTC.CTRL = RTC_PRESCALER_DIV1_gc;		// TODO: This is overriding the value set in config32KHzRTC..??
	RTC.INTCTRL = RTC_COMPINTLVL_HI_gc;
}

// TODO: This function was received from a previous group. After comparing these assignments with the register descriptions in the manual it appears the comments don't match the actions, so update the comments after testing!
void _rtc_config32KHzRTC(void){		// TODO: Actually trigger RTC compare ISR every 1ms. According to comments in the previous firmware version it doesn't yet.	-> This can probably be achieved by simply changing the RTC_MILLISECONDS define to '1'!!!
	CCP = CCP_IOREG_gc;				// Security Signature to modify clock
	OSC.CTRL |= OSC_RC32KEN_bm;		// Enable internal 32KHz oscillator

	// select RTC clk source
	CLK.RTCCTRL = 0x5;								// Internal 32KHz oscillator source RTC enable
	RTC.CTRL = CLK_RTCSRC_RCOSC_gc | CLK_RTCEN_bm;	// 1KHz / 1024 = 1 second/tick		// NOTE: This register only contains the prescaler, so use the define that specifies the needed prescale instead of the old, strange CLK_RTC defines that don't belong here!

	#ifndef SIMULATION								// Simulator hangs on this loop. It appears it is not possible to manually set this register.
	_rtc_waitUntilSynchronizationCompleted();
	#endif
}

void _rtc_waitUntilSynchronizationCompleted(void){
	while(RTC.STATUS & RTC_SYNCBUSY_bm);
}

unsigned long getMillisecondCounter(void) {
	cli();
	unsigned long counterValue = _rtc_millisecondCounter;
	sei();
	
	return counterValue;
}

void incrementMillisecondCounter(void){
	_rtc_millisecondCounter++;
}

void resetMillisecondCounter(void) {
	_rtc_millisecondCounter = 0;
}